package example.tw.sample;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.text.DecimalFormat;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String title = getResources().getString(R.string.app_title);
        Log.i("Jerry",title);
    }

    public void cal_Onclick(View view) {
        Log.i("Jerry","Onclick");

        //取得使用者相關的物件
        EditText etHeight = (EditText) findViewById(R.id.et_bodyHeight);
        EditText etWeight = (EditText) findViewById(R.id.et_bodyWeight);
        TextView tvBMI = (TextView) findViewById(R.id.tv_BMI);

        //將使用者輸入的資料存入到變數裡面
        double height = Double.parseDouble(etHeight.getText().toString());
        double weight = Double.parseDouble(etWeight.getText().toString());
        double bmi = weight / Math.pow(height/100,2);

        //JAVA 四捨五入函數
        DecimalFormat df = new DecimalFormat("##.00");
        bmi = Double.parseDouble(df.format(bmi));


        tvBMI.setText(String.valueOf(bmi));



    }

    public void bt_author_onclick(View view) {
        //切換頁面
        Intent intent = new Intent();
        intent.setClass(MainActivity.this  , SecondActivity.class);

        startActivity(intent);
    }
}
